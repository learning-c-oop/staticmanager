﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticManager
{
    class Program
    {
        static void Main()
        {
            var students = CreateStudents(40);
            Print(students);
            Console.WriteLine();
            var teachers = CreateTeachers(4);
            Print(teachers);
            Console.WriteLine();
            List<Group> groups = Manager.AddGroups(teachers, students, "C#");
            Print(groups);

            Console.ReadKey();
        }
        static List<Student> CreateStudents(int count)
        {
            List<Student> list = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                list.Add(new Student
                {
                    Name = $"A{i + 1}",
                    Surname = $"A{i + 1}yan",
                    Age = i + 1
                });
            }
            return list;
        }
        static void Print(List<Student> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"{list[i].Name}, {list[i].Surname}, {list[i].Age}");
            }
        }
        static List<Teacher> CreateTeachers(int count)
        {
            List<Teacher> teachers = new List<Teacher>(count);
            for (int i = 0; i < count; i++)
            {
                teachers.Add(new Teacher { Name = $"T{i + 1}", Surname = $"T{i + 1}yan" });
            }
            return teachers;
        }
        static void Print(List<Teacher> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"{list[i].Name}, {list[i].Surname}");
            }
        }
        static void Print(List<Group> group)
        {
            for (int i = 0; i < group.Count; i++)
            {
                Console.WriteLine();
                Console.WriteLine(group[i].Name);
                Console.WriteLine($"{group[i].Teacher.Name} {group[i].Teacher.Surname}");
                Print(group[i].Students);
                Console.WriteLine();
            }
        }
    }
}
