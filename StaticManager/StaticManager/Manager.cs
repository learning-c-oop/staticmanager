﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticManager
{
    static class Manager
    {
        public static List<Group> AddGroups(List<Teacher> teachers, List<Student> students, string name)
        {
            int groupcount = teachers.Count;
            var groups = new List<Group>(groupcount);
            foreach (var teacher in teachers)
            {
                var group = new Group { Name = name, Teacher = teacher };
                group.Students = MoveToGroup(students, students.Count / groupcount);
                groupcount--;
                groups.Add(group);
            }
            return groups;
        }
        static List<Student> MoveToGroup(List<Student> students, int count)
        {
            List<Student> groupOfStudents = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                groupOfStudents.Add(students[0]);
                students.RemoveAt(0);
            }
            return groupOfStudents;
        }
    }
}
