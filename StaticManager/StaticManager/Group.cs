﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticManager
{
    class Group
    {
        public List<Student> Students { get; set; }
        public Teacher Teacher { get; set; }
        public string Name { get; set; }
    }
}
